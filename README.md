# TU TESTING 

- Testing Framework : `MOCHA` (https://mochajs.org/)
- Assertion library : `CHAI` (http://chaijs.com/)

## Assertion Styles :
* Assert (http://chaijs.com/api/assert/)
* BDD (http://chaijs.com/api/bdd/) :
    * Expect 
    * Should


# KATA TU : Numbers in words :

Link : http://codingdojo.org/kata/NumbersInWords/ 

- clone the project : `git clone git@bitbucket.org:mlouati/numbers-in-words.git`
- install dependencies : `yarn`
- run the test task : `yarn run watch-test`


